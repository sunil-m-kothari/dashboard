<?php $start_time = microtime(); ?><!DOCTYPE html>
<html>
    <head>
        <title>Sandbox.dev Dashboard</title>
        <link rel="stylesheet" type="text/css" href="bootstrap.css">
        <meta charset="utf-8">
        <style>
            #render-time{
                position:fixed;
                bottom:0;
                right:0;
                padding:2px 5px;
                background-color: rgba(255,255,255,.7);
                border-top-left-radius: 4px;
            }
            #masonry{
                -webkit-columns: 400px;
                   -moz-columns: 400px;
                        columns: 400px;
            }
            #masonry .panel{
                margin:20px 0;
                -webkit-column-break-inside: avoid;
                          page-break-inside: avoid;
                               break-inside: avoid;
            }
        </style>
    </head>
    <body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 page-header"><h1 class="xpage-header">Sandbox.dev Dashboard</h1><p class="lead">VM Ubuntu Server 14.04 LTS</p></div>
        </div>
        <div id="masonry">
        <?php

            //initialize connection
            $dsn = 'mysql:host=sandbox.dev;dbname=dashboard';
            try{
                $db = new PDO($dsn, 'dashboard', 'dashboard');
            }
            catch(Exception $e){
                echo "<strong>Connection failed</strong>";
                exit();
            }

            //Display panels by type
            $sql = 'SELECT GROUP_CONCAT(id) as id, GROUP_CONCAT(path) as path, GROUP_CONCAT(label) as label, type from shortcuts GROUP BY type';
            $result = $db->query($sql, PDO::FETCH_ASSOC);
            foreach ($result as $row){
                echo bootstrapPanel($row['type'], $row);
            }
        ?>
        </div>
    </div>
    </body>
</html><?php



function bootstrapPanel($title, $row){
    $html = '
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">'.$title.'</h3>
                </div>
                <div class="list-group">';
                
    $id = explode(',', $row['id']);
    $path = explode(',', $row['path']);
    $label = explode(',', $row['label']);

    foreach ($id as $index => $value) {
            $current_path = filter_var($path[$index], FILTER_SANITIZE_URL);
            $current_label = filter_var($label[$index], FILTER_SANITIZE_STRING);
            $html .= sprintf("\n\t\t    <a class='list-group-item' href='%s'>%s</a>",$current_path, $current_label);
    }

    $html .= '
                </div>
            </div>
';

    return $html;
}

echo '<div id="render-time">Time taken: ' . round((microtime() - $start_time),5) . ' s</div>';